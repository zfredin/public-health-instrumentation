# Public Health Instrumentation

A collection of open instruments to help the pandemic recovery effort, supplementing biological testing with widespread early warning systems.

## explorations

[pulse oximetry](pulseox/README.md)

[infrared temperature measurement](irtemp.md)

[airflow validation](airflowvalidation.md)

[respiration sensing](respirationsensing.md)
