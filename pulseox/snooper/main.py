# Image Statistics Info Example
#
# This script computes the statistics of the image and prints it out.

import sensor, image, time, ustruct
from pyb import UART

sensor.reset()
sensor.set_pixformat(sensor.GRAYSCALE) # or RGB565.
sensor.set_framesize(sensor.QVGA)
sensor.skip_frames(time = 2000)
sensor.set_auto_gain(False) # must be turned off for color tracking
sensor.set_auto_whitebal(False) # must be turned off for color tracking
clock = time.clock()
uart = UART(3, 19200)
seg_thresh = 60
SpO2 = 0;

#        ---A1---     ---A2---
#       |        |   |        |
#       F1       B1  F2       B2
#       |        |   |        |
#        ---G1---     ---G2---
#       |        |   |        |
#       E1       C1  E2       C2
#       |        |   |        |
#        ---D1---     ---D2---

while(True):
    clock.tick()
    SpO2 = 0;
    img = sensor.snapshot()

    # segment 1 (left)
    if(img.get_statistics(roi=(43,50,8,20)).mean() > seg_thresh): #F1
        if(img.get_statistics(roi=(45,131,6,17)).mean() > seg_thresh): #E1
            if(img.get_statistics(roi=(77,96,18,7)).mean() > seg_thresh): #G1
                if(img.get_statistics(roi=(117,50,9,17)).mean() > seg_thresh): #B1
                    SpO2 += 80
                else:
                    SpO2 += 60
            else:
                SpO2 += 0
        elif(img.get_statistics(roi=(79,174,15,8)).mean() > seg_thresh): #D1
            if(img.get_statistics(roi=(117,50,9,17)).mean() > seg_thresh): #B1
                SpO2 += 90
            else:
                SpO2 += 50
        else:
            SpO2 += 40
    elif(img.get_statistics(roi=(77,96,18,7)).mean() > seg_thresh): #G1
        if(img.get_statistics(roi=(70,16,22,10)).mean() > seg_thresh): #A1
            if(img.get_statistics(roi=(45,131,6,17)).mean() > seg_thresh): #E1
                SpO2 += 20
            else:
                SpO2 += 30
        else:
            SpO2 = 0
    elif(img.get_statistics(roi=(70,16,22,10)).mean() > seg_thresh): #A1
        SpO2 += 70
    elif(img.get_statistics(roi=(117,50,9,17)).mean() > seg_thresh): #B1
        SpO2 += 10
    else:
        SpO2 += 0

    # segment 2 (right)
    if(img.get_statistics(roi=(154,47,10,22)).mean() > seg_thresh): #F1
        if(img.get_statistics(roi=(156,133,10,19)).mean() > seg_thresh): #E1
            if(img.get_statistics(roi=(188,94,18,8)).mean() > seg_thresh): #G1
                if(img.get_statistics(roi=(234,43,8,22)).mean() > seg_thresh): #B1
                    SpO2 += 8
                else:
                    SpO2 += 6
            else:
                SpO2 += 0
        elif(img.get_statistics(roi=(188,176,21,8)).mean() > seg_thresh): #D1
            if(img.get_statistics(roi=(234,43,8,22)).mean() > seg_thresh): #B1
                SpO2 += 9
            else:
                SpO2 += 5
        else:
            SpO2 += 4
    elif(img.get_statistics(roi=(188,94,18,8)).mean() > seg_thresh): #G1
        if(img.get_statistics(roi=(183,13,21,8)).mean() > seg_thresh): #A1
            if(img.get_statistics(roi=(156,133,10,19)).mean() > seg_thresh): #E1
                SpO2 += 2
            else:
                SpO2 += 3
        else:
            SpO2 = 0
    elif(img.get_statistics(roi=(183,13,21,8)).mean() > seg_thresh): #A1
        SpO2 += 7
    elif(img.get_statistics(roi=(234,43,8,22)).mean() > seg_thresh): #B1
        SpO2 += 1
    else:
        SpO2 += 0
    uart.write(ustruct.pack("<b", SpO2))
    print(SpO2)

# print(clock.fps())

# You can also pass get_statistics() an "roi=" to get just the statistics of that area.
# get_statistics() allows you to quickly determine the color channel information of
# any any area in the image.
