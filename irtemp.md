## Infrared Temperature Monitoring
Infrared (IR) temperature monitoring is a common non-contact measurement technique used in a variety of industries and settings. IR temperature instruments span a range from cheap handheld point sensors to sophisticated microbolometer arrays that can resolve thermal images. In all cases, the technology relies on measuring the intensity of infrared light and correlating that value to the [blackbody radiation](https://en.wikipedia.org/wiki/Black-body_radiation) curve with an assumed [emissivity](https://en.wikipedia.org/wiki/Emissivity).

### References

### Operational Theory

### Practical Considerations
